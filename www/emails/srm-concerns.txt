From: Martin Zobel-Helas <zobel@debian.org>
To: Debian Release Mailinglist <debian-release@lists.debian.org>,
    Debian Devel Mailinglist <debian-devel@lists.debian.org>
Subject: [SRM] concerns about upcoming stable architectures

Hi,

as per mail from Andi Barth [1], SRM may have concerns about the current
list of architectures for the upcoming stable release, which might lead to a
block for this architecture to be in the list of releaseable architectures.

The current list is as follows:

+ arm:	there are no developer accessible machines at the moment. 
	    europa.debian.org   status: locked down
	    elara.debian.org    status: locked down
	    agnesi.debian.org   status: being setup, host not reachable
	    leisner.debian.org  status; unkown, host not reachable

+ mipsel: The mipsel stable-security buildd(s) have issues accessing the
	wanna-build database, documented by Moritz in RT#164

+ sparc: There are currently issues with binutils/glibc/kernel with current
	stable sparc kernels, which lead to a very unstable buildd bahavoir.
	This is documented in BTS#XXXXXX

Greetings
Martin


[1] http://lists.debian.org/debian-devel-announce/2007/06/msg00005.html
--

