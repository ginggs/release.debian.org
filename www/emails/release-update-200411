To: debian-devel-announce@lists.debian.org
Subject: Release update: GNOME 2.8, yes; freeze date waiting for infrastructure
Date: 2004-11-30

Hello, world.

After almost a month since the last update, the status of the release is
as follows.


After requests and a detailed proposal from the GNOME team, we accepted
an upload of GNOME 2.8 into sid, and, via the usual mechanisms, into
sarge.  We should mention that the release team was running out of
objections to GNOME 2.8 in unstable that the GNOME team hasn't
satisfactorily addressed; this, and the fact that they have demonstrated
good reaction times of late are the main reasons why we're approving it
despite the timing.


In the meantime, we were also asked why we decided to go with KDE 3.2,
and if it would be possible to go with KDE 3.3 instead.  The main reason
is that KDE 3.3 in unstable started with some RC bugs, and there was no
proposal from the KDE team how to proceed.  The door is only closed, but
not locked for KDE 3.3.  We are still open for proposals how to sort the
KDE 3.3 issues out, and there has been some productive discussion of
late about that - but no final decision yet.

For KDE, as for GNOME, we need to be sure that that change doesn't
negatively affect Debian's releasability.  This includes, but is not
limited to:

  * Ensure that the package relationships are correct (which calls for
    comprehensive partial upgrade testing).

  * Research and document any partial upgrade implications: we must not
    get into a state where part of KDE 3.3 is promoted to testing in
    such a way as to make sarge unreleasable.

  * Make sure that there is no bad effect on other parts of the system.
    In particular, CD size (such as any growth in the size of a system
    installed with 'tasksel install desktop') comes to mind.

We'd also rather not try to deal with both a substantial GNOME upgrade
and a substantial KDE upgrade at the same time, for obvious reasons
involving the release team's sanity. :-)

For details, we will of course continue our existing mail threads, but
we want to give all developers an overview of what's involved in that
decision.


The toolchain was updated again: A new version of binutils was accepted
to fix a regression on m68k. Also, we have an update of gcc-3.4 pending
that was intended to fix gjc on mips, but it will require some more
time. We don't expect any of this to become a release blocker, but we
want to keep you informed.


The bad news is that we still do not have an ETA for the
testing-security autobuilders to be functional.  This continues to be
the major blocker for proceeding with the freeze; we would /like/ to
have security support in place for sarge before encouraging widespread
upgrade woody->sarge upgrade testing, but we /need/ to have it in place
before releasing, so it would be unwise to try to freeze the rest of the
archive without any confirmed schedule for the last stages of the
release.

Getting testing-security up and running is blocking on a couple of
changes to the archive configuration.  Security uploads are first
uploaded to their own archive while the security announcement is in
preparation, and later (via a tool called 'amber') uploaded to the main
archive.  At the moment, it would be possible for an upload to be
accepted by the security archive but then rejected by the main archive
due to the automatic requirement that all packages in testing-security
have a version no greater than that in unstable.  Losing security
uploads this way isn't acceptable, so there has been discussion among
the archive maintenance team about how to fix this.  A pseudocode
solution has been constructed and is now being implemented.  Once that's
done, we should see testing-security support soon afterwards.

We are also still missing official autobuilders for
testing-proposed-updates on alpha and mips.  All other architectures
appear to be keeping up with t-p-u uploads.  This is causing some
moderate delays getting fixes into testing via t-p-u.  If you have
specific concerns about packages that you've uploaded to t-p-u, please
contact debian-release@lists.debian.org.  But, normally, packages
should continue to go in via unstable if at all possible (please see
below).


As for some good news, last weekend saw several simultaneous
Bug-Squashing Parties.  They managed not only to squash some bugs, but
also to tackle some more difficult problems: we know now that we
probably need updated modutils for upgrades on hppa with 64-bit kernels.
Since kernel upgrade issues are a major remaining blocker, this is good
progress.


Package uploads for sarge should continue to be made according to the
following guidelines (that were also mentioned in the last two updates):

  * If your package is frozen, but there are no changes in unstable that
    should not also be in testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.

  * If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.

  * If your package depends on KDE and you have changes you need to make
    to fix important or RC bugs for sarge, you will need to upload to
    testing-proposed-updates as well, with the same requirements as for
    frozen packages.

  * All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.




Keep up the good work, folks.


Cheers,
-- 
NN
Debian Release Team
