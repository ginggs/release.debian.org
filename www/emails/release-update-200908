To: debian-devel-announce@lists.debian.org
Subject: Bits from the release team: Release goals, schedule, state of the union

Heya,

This mail should be the first in a row of roughly monthly mails 
informing the project about the state of the release. Please don't
hesitate to contact us on debian-release@lists.debian.org whenever
you have questions.

Release Goals
=============
We have now reviewed the list of release goals [RT-Goals] and have
ACKed most of the proposed things. A short overview:

 - multiarch
    We hope to allow our users to install binaries for several 
    architectures on a single machine in Squeeze.
 - kFreeBSD: 
    Debian 6.0 Squeeze should be the first Debian release shipping with
    a non-Linux kernel.
 - Boot performance: 
    Using a minimal shell such as dash, together with the possibility to
    use an advanced init script management system, should make booting 
    Debian even faster.
 - Package quality: 
    We want to encourage the use of tools such as piuparts and lintian
    to improve our packages's quality.
 - Removal of obsolete libs: 
    It's time to get rid of Gnome 1.x cruft and similar old, now 
    unneeded libraries.
 - IPv6/LFS: 
    Further support for IPv6 and large files is still a release goal.
 - New source package format:
    Make it possible to build all packages using dpkg source format
    3.0 (quilt).
 - Removal of .la files:
    To avoid future problems, we want to remove the error-prone libtool
    .la files wherever they are not used.

There are some other goals in the queue for which we would like to
have a bit more information; individual mails requesting that have been
sent out. If you know of something you want to see as release goal for
Squeeze, mail us on debian-release@.


Release planning
================
As you may have noticed, we have asked for your development plans
for the Squeeze release cycle [RT-Survey]. We haven't received replies
from everyone yet (if you want to reply, do so soon) and still haven't
decided on a final schedule but, at this moment, a freeze in December
seems to be unlikely. We will send another mail when we have decided
on a schedule.


Release state
=============
The current state of unstable and testing isn't very good and we would
like to change this with your help. The problems are two-fold: We have
a very high number of release critical bugs and had problems ensuring
the smooth transition of packages to testing.

We have intensified our work on supporting transitions in the past 
weeks, allowing quite a few packages to move to testing. We will 
continue to do so in the coming weeks and would be grateful if you could
announce every non-trivial transition to debian-release@, so that we
can plan for it.

There were also some buildd problems in the last few days, but we hope
we are past them. We are currently working on the list of architectures
that will be released with Squeeze [RT-Archs] and hope to finalize it
soon.

The number of release critical bugs is something that requires help
from all of you. Please check your packages using our QA pages [DDPO]
and packages on your system using ``rc-alert'' (from the devscripts
package) and fix any outstanding bugs. If you notice bugs in 
unmaintained packages, consider informing the MIA team (at 
mia@qa.debian.org) or requesting the removal of the package.

Release preparations
====================
To encourage help from new developers, we would like to organize some
QA weekends (in the style of Gnome Love Days [GnomeLove]), preferably
with a real-life event at the same time.

For this, we will name a specific area which requires attention (for
example, bugs concerning a release goal) and provide documentation on how
to work on these issues. We hope to attract enough DDs to make it 
possible to give advice to new helpers and to get all fixes sponsored in
a timely fashion.

If you would like to help with this and are willing to organize a real
life event, mail us on debian-release@, so that we have some specific
dates to work with.

Cheers,
Marc

Footnotes:
 [RT-Goals] http://release.debian.org/squeeze/goals.txt
 [RT-Survey] http://lists.debian.org/debian-devel/2009/08/msg00602.html
 [RT-Archs] http://release.debian.org/squeeze/arch_qualify.html
 [DDPO] http://qa.debian.org/developer.php
 [GnomeLove] http://live.gnome.org/GnomeLove
-- 
http://release.debian.org
Debian Release Team
