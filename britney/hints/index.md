#! TITLE: Debian Release Management
#! SUBTITLE: quando paratus est

# Documentation

 * [hints](../../doc/britney/hints.html)

# User hints files

 * [adsb](adsb)
 * [elbrus](elbrus)
 * [ginggs](ginggs)
 * [ivodd](ivodd)
 * [jcristau](jcristau)
 * [jmw](jmw)
 * [kibi](kibi)
 * [pochu](pochu)
 * [sramacher](sramacher)

# Role hint files

 * [auto-removals](auto-removals)
 * [freeze](freeze)
 * [freeze-exception](freeze-exception)

# Other relevant control files

 * [transitions.yaml](https://ftp-master.debian.org/transitions.yaml)

