#! /bin/sh

# Wrapper script to perform tasks related to the generation and maintenance
# of https://release.debian.org/proposed-updates/

# Copyright 2016-7 Adam D. Barratt <adam@adam-barratt.org.uk>
# (as much as that makes sense...)

# Purely to avoid repeating the path in each of the following invocations
cd /srv/release.debian.org/tools

LOCKFILE="/srv/release.debian.org/tmp/queue-viewer.lock"

(
flock -n 9 || exit 1

LOGFILE="/srv/release.debian.org/var/data/queue-viewer/logs/cron-$(date +%Y-%m)"

log() {
  printf "%s %s\n" "$(date "+%Y-%m-%d %H:%M:%S")" "$1" >> ${LOGFILE}
}

log "Running stable-compare"
./stable-compare/get-Packages.sh stable
./stable-compare/get-Packages.sh oldstable
./stable-compare/compare.py stable
./stable-compare/compare.py oldstable
log "stable-compare done"

log "Syncing comments from ftp-master"
# Pull in any updated comments from ftp-master
./scripts/sync-pu-comments
log "Comment synced"

log "Copying symlink trees"
# Copy the symlink trees for the -new queues, to avoid issues if they
# change under us
rsync -a --delete /srv/ftp-master.debian.org/queue/p-u-new/export/ \
    /srv/release.debian.org/tmp/p-u-new/
rsync -a --delete /srv/ftp-master.debian.org/queue/o-p-u-new/export/ \
    /srv/release.debian.org/tmp/o-p-u-new/
log "Symlink trees done"

log "Running queue-viewer"
# Run queue-viewer itself
## Force a UTF-8 locale so that debdiffs are correctly formatted
## Force an explicit TMPDIR as larger (e.g. chromium-browser) debdiffs may
##   overflow the system's /tmp
## Force release's bin folder to be first on the path, so that the local
##   copy of debdiff is used in preference to the system copy
LC_ALL=C.UTF-8 TMPDIR=/srv/release.debian.org/tmp PATH=/srv/release.debian.org/bin:$PATH \
    nice -n 20 \
    ./queue-viewer/queue-viewer \
    /srv/release.debian.org/etc/queue_viewer.conf
log "queue-viewer done"

log "Syncing website"
# Deploy the updated web pages to the release.debian.org site
# (via static.debian.org)
chronic static-update-component release.debian.org-pu
log "Website sync complete"

rm "${LOCKFILE}"

) 9>"${LOCKFILE}"
